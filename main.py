# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from config import *

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    if "music" in text:
        url = "https://music.bugs.co.kr/chart/"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        keywords = ["Bugs 실시간 음악 차트 Top10"]
        keywords.append("-----------------------------------")
        title = []
        artist = []

        for data in (soup.find_all("p", class_="title")):
            if len(title) >= 10:
                break
            title.append(data.find("a").get_text().strip())

        for data in soup.find_all("p", class_="artist"):
            if len(artist) >= 10:
                break
            artist.append(data.find("a").get_text().strip())

        for i in range(10):
            final = str(i + 1) + "위 : " + title[i] + " / " + artist[i]
            keywords.append(final)

        return u'\n'.join(keywords)
    elif "musical" in text:

        url = "http://ticket.interpark.com/contents/Ranking?pType=W"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        musical_list = []
        musical_keywords = ["인터파크 뮤지컬 주간 랭킹 Top10"]
        musical_keywords.append("-----------------------------------")
        musical_title = []
        musical_date = []
        musical_price = []
        #.find('a')['href'])
        # for i in soup.find_all("div", class_="mfn_inner"):
        #     list.append("https://news.sbs.co.kr" + i.find('a')['href'])

        for i in (soup.find_all("div", class_="itemWrap custom_01011")):

            for j in i.find_all("div", class_="percentage"):
                temp = j.find('a')['onclick'].split("'")
                #print(temp[1])
                musical_list.append(temp[1])
            #print(musical_list)

            for j in (i.find_all("a", class_="prdName")):
                if len(musical_title) >= 10:
                    break
                    #뮤지컬《엑스칼리버》
                temp =j.get_text().replace("《","").replace("》","").replace("〈","").replace("〉","").strip().split('뮤지컬')
                #temp = temp.replace
                musical_title.append(temp[1].strip())

            for j in (i.find_all("a", class_="prdDuration")):
                if len(musical_date) >= 10:
                    break
                musical_date.append(j.get_text().strip())


        for i in range(10):
            final = str(i + 1) + "위 : " + musical_title[i] + " / "+ musical_date[i]
            musical_keywords.append(final)

        return u'\n'.join(musical_keywords)

    elif "concert" in text:
        url = "http://ticket.interpark.com/contents/Ranking?pType=W"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        concert_keywords = ["인터파크 콘서트 주간 랭킹 Top10"]
        concert_keywords.append("-----------------------------------")
        concert_title = []
        concert_date = []

        for i in (soup.find_all("div", class_="itemWrap custom_01003")):
            for j in (i.find_all("a", class_="prdName")):
                if len(concert_title) >= 10:
                    break
                concert_title.append(j.get_text().strip())
            for j in (i.find_all("a", class_="prdDuration")):
                if len(concert_date) >= 10:
                    break
                concert_date.append(j.get_text().strip())


        for i in range(10):
            final = str(i + 1) + "위 : " + concert_title[i] + " / "+ concert_date[i]
            concert_keywords.append(final)

        return u'\n'.join(concert_keywords)
    # elif "movie" in text:
    #     url = "https://movie.daum.net/main/new#slide-1-0"
    #     source_code = urllib.request.urlopen(url).read()
    #     soup = BeautifulSoup(source_code, "html.parser")
    #
    #     keywords = ["실시간 영화 예매 순위 Top10"]
    #     keywords.append("-----------------------------------")
    #     movie_title = []
    #
    #     for i in (soup.find_all("ul", class_="list_movie #movie")):
    #         for j in (i.find_all("div", class_="info_tit")):
    #             if len(movie_title) >= 10:
    #                 break
    #             movie = j.find("a").get_text()
    #             movie_title.append(movie)
    #     print(movie_title)
    #     for i in range(10):
    #         final = str(i + 1) + "위 : " + movie_title[i]
    #         keywords.append(final)
    #
    #     return u'\n'.join(keywords)
    else :
        return '입력해주세요'


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message = _crawl_music_chart(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=message
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
